<?php

declare(strict_types=1);

namespace Passcreator\JobQueue\GooglePubSub\Queue;

use Flowpack\JobQueue\Common\Exception as JobQueueException;
use Flowpack\JobQueue\Common\Queue\Message;
use Flowpack\JobQueue\Common\Queue\QueueInterface;
use Google\Cloud\PubSub\PubSubClient;
use Google\Cloud\PubSub\Subscription;
use Google\Cloud\PubSub\Topic;
use Neos\Flow\Annotations as Flow;

class GooglePubSubQueue implements QueueInterface {

    /**
     * @var int
     */
    protected $defaultTimeout = null;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var PubSubClient
     */
    protected $pubSubClient;

    /**
     * @var array
     */
    protected $serviceAccountConfig;

    /**
     * @var Topic
     */
    protected $topic;

    /**
     * The acknowledgement deadline in seconds
     *
     * @var int
     */
    protected $ackDeadline = 10;

    /**
     * The regions where this queue may be persisted.
     * See https://cloud.google.com/compute/docs/regions-zones/?hl=en#locations
     * for a list of valid regions
     *
     * @var array
     */
    protected $allowedPersistenceRegions;

    /**
     * @param mixed[] $options
     */
    public function __construct(string $name, array $options = []) {
        $queuePrefix = $options['queuePrefix'] ?? '';

        $this->ackDeadline = $options['ackDeadline'] ?? 10;

        // maximum number of seconds a message may take to acknowledge is 600 seconds
        if ($this->ackDeadline > 600) {
            $this->ackDeadline = 600;
        }

        $this->allowedPersistenceRegions = $options['allowedPersistenceRegions'] ?? array('europe-west1');

        $this->name = $queuePrefix . $name;

        $serviceAccountConfig = $options['serviceAccountConfiguration'] ?? '';

        if ($serviceAccountConfig === '') {
            throw new JobQueueException('Make sure to specify the service account credentials JSON as a base 64 encoded string in the queue options using the parameter serviceAccountConfiguration!');
        }

        $this->serviceAccountConfig = json_decode(base64_decode($serviceAccountConfig), true);

        $this->pubSubClient = new PubSubClient([
            'keyFile' => $this->serviceAccountConfig
        ]);

        $this->topic = $this->pubSubClient->topic($this->name);
    }

    /**
     * @inheritdoc
     */
    public function abort(string $messageId): void {
        // aborting a message simply means not acknowledging it
    }

    public function countFailed(): int {
        return 0;
    }

    public function countReady(): int {
        return $this->count();
    }

    /**
     * @inheritdoc
     */
    public function count() {
        return 0;
//        return (int)$this->channel->queue_declare($this->name, true)[1];
    }

    public function countReserved(): int {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function flush(): void {
        throw new JobQueueException('Not implemented');
    }

    public function getName(): string {
        return $this->name;
    }

    /**
     * @throws JobQueueException
     *
     * @inheritdoc
     */
    public function peek(int $limit = 1): array {
        throw new JobQueueException('Not implemented');
    }

    /**
     * Connected to the "messageReleased" Signal
     *
     * @param mixed[] $releaseOptions
     *
     */
    public function reQueueMessage(Message $message, array $releaseOptions): void {
        // Ack the current message
        $this->finish($message->getIdentifier());

        // requeue the message
        $this->queue($message->getPayload(), $releaseOptions, $message->getNumberOfReleases() + 1);
    }

    /**
     * @inheritdoc
     */
    public function finish(string $messageId): bool {
        $this->getSubscription()->acknowledge(new \Google\Cloud\PubSub\Message(array(), array('ackId' => $messageId)));
        return true;
    }

    /**
     * @return \Google\Cloud\PubSub\Subscription
     */
    protected function getSubscription(): Subscription {
        return $this->pubSubClient->subscription($this->name, $this->name);
    }

    /**
     * @param mixed[] $options
     */
    protected function queue(string $payload, array $options = [], int $numberOfReleases = 0): string {
        // Publish a message to the topic.
        $messageIds = $this->topic->publish([
            'data' => $payload,
            'attributes' => [
                'scheduled' => serialize($this->getScheduledParameter($options))
            ]
        ]);

        return json_encode($messageIds);
    }

    /**
     * @param array $options
     *
     * @return \DateTime
     * @throws \Exception
     */
    protected function getScheduledParameter(array $options): \DateTime {
        if (!isset($options['delay'])) {
            return new \DateTime();
        }

        $delay = $options['delay'];
        $scheduledOn = new \DateTime();
        $scheduledOn->modify("+{$delay} seconds");
        return $scheduledOn;
    }

    /**
     * @param mixed[] $options
     */
    public function release(string $messageId, array $options = []): void {
        // TODO: check if another mechanism works with Pub/Sub. This is a leftover of the RabbitMQ package but still works.
        // We will listen on the "messageReleased" signal. This signal has the fill
        // $message available. So we will ack the origin message and queue a new message
        // with the $same payload
    }

    public function setUp(): void {
        try {
            $this->topic->create([
                'name' => $this->name,
                'messageStoragePolicy' => [
                    'allowedPersistenceRegions' => $this->allowedPersistenceRegions
                ]
            ]);
            $this->getSubscription()->create(array('ackDeadlineSeconds' => $this->ackDeadline));
        } catch (\Exception $e) {
            $this->topic->update([
                'name' => $this->name,
                'messageStoragePolicy' => [
                    'allowedPersistenceRegions' => $this->allowedPersistenceRegions
                ]
            ]);
        }
    }

    public function shutdownObject(): void {
    }

    /**
     * @param mixed   $payload
     * @param mixed[] $options
     *
     */
    public function submit($payload, array $options = []): string {
        return $this->queue($payload, $options);
    }

    public function waitAndReserve(?int $timeout = null): ?Message {
        return $this->dequeue(false);
    }

    protected function dequeue(bool $ack = true): ?Message {
        // Get an instance of a previously created subscription.
        $subscription = $this->getSubscription();

        // Pull all available messages.
        $messages = $subscription->pull(array('maxMessages' => 1));

        $message = null;

        if (!empty($messages)) {
            $pubSubMessage = $messages[0];

            $scheduled = unserialize($pubSubMessage->attribute('scheduled'));

            if ($scheduled <= new \DateTime()) {
                $message = new Message($pubSubMessage->ackId(), $pubSubMessage->data(), 0);
            } else {
                // if scheduled date is in the future, simply ignore the message.
                // It'll be retried later in this case
                return null;
            }
        }

        if ($ack) {
            $subscription->acknowledge($messages[0]);
        }

        return $message;

//
//        foreach ($messages as $message) {
//            echo $message->id();
//            echo $message->data() . "\n";
//            echo $message->attribute('location');
//        }
    }

    public function waitAndTake(?int $timeout = null): ?Message {
        return $this->dequeue(true);
    }

}
